<?php

// Boolean

$boolTrue = true;
$boolFalse = false;
$string = 'String';
$integer = 'Integer';

$action = "123"; // String
$checkString = $action == "123";
if ($checkString) {
    echo "$string. == Перевірка пройшла вдало";
    echo '<br>';
}

if ($action == 123) {
    echo "{$integer}s. == Перевірка пройшла вдало";
    echo '<br>';
}

if ($action === "123") {
    echo '$string. === Перевірка пройшла вдало';
    echo '<br>';
} else {
    echo '$string. === Перевірка не пройшла';
    echo '<br>';
}

// string !== integer
if ($action === 123) {
    echo '$integer. === Перевірка пройшла вдало';
    echo '<br>';
} else {
    echo '$integer. === Перевірка не пройшла';
    echo '<br>';
}

// Перевірка чи це буленове значення
var_dump(is_bool($action === 123));
// Приведння до буленового значення
var_dump(boolval($action));
var_dump((bool) $action);
var_dump((boolean) $action);



//qweqweqweqwe
// Integer
$large_integer = 2147483647;
var_dump($large_integer); // int
$large_integer *= 21474836472147483647;
var_dump($large_integer);

